const cheerio = require("cheerio");
const fs = require("fs-extra");
const path = require("path");
const he = require("he");
const textract = require("textract");

if (!process.argv[2]) {
  console.error("Укажите подпапку в папке files");
  process.exit();
}

files_path = path.join(__dirname, "files", process.argv[2]);

let config = {
  preserveLineBreaks: true
};

let html = fs.readFileSync(path.join(files_path, "output_for_translate.html"));
// let text = fs.readFileSync(path.join(files_path, "translation.txt")).toString();

const $ = cheerio.load(html);

textract.fromFileWithPath(files_path + "/translation.docx", config, function(error, text) {
  let rows = {};
  for (let row of text.split("\n\n")) {
    try {
      let num = row.split(' " ')[0];
      let row_text = row.split(' " ')[1];
      rows[num] = row_text;
    } catch (e) {
      console.log("Syntax error on the row: ", row);
    }
  }

  $("[data-translate]").each(function() {
    let num = $(this).attr("data-translate");
    let row_text = rows[num];
    $(this).replaceWith(row_text);
  });

  try {
    fs.writeFileSync(
      path.join(files_path, "translated.html"),
      he.decode($.html())
    );
    console.log("Готово!");
  } catch (error) {
    console.log('Ошибка записи файла');
  }
});
