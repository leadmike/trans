const cheerio = require("cheerio");
const fs = require("fs-extra");
const path = require("path");
const he = require("he");
const docx = require("docx");
const argv = require("yargs").argv;

let outputName =
  argv.filename && argv.filename.length > 0 ? argv.filename : "extracted";

let doc = new docx.Document();

if (!process.argv[2]) {
  console.error("Укажите подпапку в папке files");
  process.exit();
}
files_path = path.join(__dirname, "files", process.argv[2]);

let html = fs.readFileSync(path.join(files_path, "index.html"));
const $ = cheerio.load(html);

let output = "";

let node_index = 0;

check_children($("html")[0]);

function check_children(root) {
  $(root.children).each(function() {
    if ($(this)[0].type == "text") {
      if (["script", "style"].indexOf($(this)[0].parent.name) != -1) {
        return;
      }
      let text = $(this).text();
      let start_whites = "";
      let end_whites = "";
      let real_text = text.match(/\S+(.|\n)+\S+/);
      if (real_text) {
        real_text = real_text[0];
      } else {
        real_text = "";
      }

      if (real_text) {
        start_whites = text.split(real_text)[0];
        end_whites = text.split(real_text)[1];
        node_index++;
        const wrap = $(
          `${start_whites}<span data-translate="${node_index}">${real_text}</span>${end_whites}`
        );
        $(this).replaceWith(wrap);
        output += `${node_index} | ${real_text}\n\n`;
        doc.createParagraph(`${node_index} | ${real_text}`);
        doc.createParagraph();
      }
    } else {
      check_children($(this)[0]);
    }
  });
}

let exporter = new docx.LocalPacker(doc);

try {
  exporter.pack(path.join(files_path, outputName));
  fs.writeFileSync(
    path.join(files_path, "output_for_translate.html"),
    he.decode($.html())
  );
  console.log("Готово!");
} catch (error) {
  console.log("Ошибка записи файла");
}
